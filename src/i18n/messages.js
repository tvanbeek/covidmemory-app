export default {
  en: {
    actions: {
      accept: 'accept',
      download: 'download original file',
      submit: 'submit',
      createMemory: 'Add my memory!',
      previousMemory: 'previous memory:',
      nextMemory: 'next memory:',
      share: 'share:',
    },
    createdBy: 'by <b>{creator}</b>',
    usefulLinks: 'quick links:',
    project: {
      title: '#covidmemory',
      more: '(read more about this project)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send your picture or videos. Fields marked with *',
      },
      intro: {
        title: 'COVID-19 <span class="h">memories</span>',
        subheading: `A platform to collect COVID19 related photos, videos, stories and interviews
        from/with ordinary people living or working in Luxembourg`,
        covidAlert: `If you have questions about COVID19 in general or
        your personal health and wellbeing please consult
        <a target="_blank" href="https://coronavirus.gouvernement.lu/en.html">covid19.lu</a>`,
      },
      terms: {
        title: 'Terms of use',
        subheading: `Consultation of this site is
        subject to the terms and conditions set out below.`,
      },
      about: {
        title: 'About',
        subheading: `<b>covidmemory.lu</b> is a free and open online platform to
        collect memories of the COVID19 crisis in Luxembourg`,
      },
      notFound: {
        title: 'Not found',
        subheading: '',
      },
      memories: {
        title: 'Memories.',
        total: 'loading... | 1 memory | {n} memories',
      },
      memory: {
        share: 'Help us collecting memories! covidmemory.lu',
      },
      map: {
        title: 'Map.',
      },
      privacy: {
        title: 'Data protection',
        subheading: '',
      },
    },
    to: {
      index: 'home',
      memories: 'memories',
      about: 'about',
      intro: 'introduction',
      map: 'map',
      create: 'add yours',
      terms: 'terms of use',
      privacy: 'Privacy',
    },
    languages: 'languages',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
      lb: 'Lëtzebuergesch',
    },
    welcome: 'Welcome!',
  },
  fr: {
    actions: {
      accept: 'Accepter',
      download: 'download original file',
      submit: 'Soumettre',
      createMemory: 'Ajouter ma contribution',
      previousMemory: 'previous memory :',
      nextMemory: 'next memory :',
      share: 'partager :',
    },
    createdBy: 'par <b>{creator}</b>',
    usefulLinks: 'liens directs:',
    project: {
      title: '#covidmemory',
      more: '(en savoir plus sur le projet)',
    },
    views: {
      create: {
        title: 'Ajouter mon souvenir à la collection',
        subheading: 'compléter le formulaire avant de l\'envoyer',
      },
      intro: {
        title: 'Souvenirs du COVID-19',
        subheading: `La plateforme pour collecter photos, vidéos, récits et
        témoignages, liés au COVID-19, de personnes habitant ou
        travaillant au Luxembourg.`,
        covidAlert: `Si vous avez des questions sur le COVID19 en général ou
        sur votre santé personnelle, veuillez consulter
        <a target="_blank" href="https://coronavirus.gouvernement.lu/fr.html">covid19.lu</a>`,
      },
      terms: {
        title: 'Conditions d\'utilisation',
      },
      about: {
        title: 'À propos',
        subheading: `<b>covidmemory.lu</b> est une plateforme en ligne libre et
        ouverte pour collecter des souvenirs de la crise du Covid19 au Luxembourg.`,
      },
      notFound: {
        title: 'Non trouvé',
        subheading: '',
      },
      memories: {
        title: 'Souvenirs.',
        total: 'chargement... | 1 souvenir trouvé | {n} en total',
      },
      memory: {
        share: 'Help us collecting memories! covidmemory.lu',
      },
      map: {
        title: 'Carte.',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'accueil',
      memories: 'souvenirs',
      about: 'à propos',
      intro: 'introduction',
      create: 'ajoutez vos souvenirs!',
      map: 'carte',
      terms: 'conditions d\'utilisation',
      privacy: 'Vie privée',
    },
    languages: 'langues',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
      lb: 'Lëtzebuergesch',
    },
    welcome: 'Bienvenue!',
  },
  de: {
    actions: {
      accept: 'accept',
      download: 'download original file',
      submit: 'submit',
      createMemory: 'Beitrag einreichen',
      previousMemory: 'previous memory:',
      nextMemory: 'next memory:',
      share: 'share:',
    },
    createdBy: 'by <b>{creator}</b>',
    usefulLinks: 'quick links:',
    project: {
      title: '#covidmemory',
      more: '(mehr über das Projekt)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send',
      },
      intro: {
        title: 'COVID-19 Erinnerungen',
        subheading: `Plattform zur Sammlung von Fotos,
        Videos und Geschichten zur COVID-19 Krise in Luxemburg.`,
        covidAlert: `Falls Sie Fragen zu COVID19 oder Ihrer
        persönlichen Gesundheit haben, besuchen Sie bitte
        <a target="_blank" href="https://coronavirus.gouvernement.lu/de.html">covid19.lu</a>`,
      },
      terms: {
        title: 'Nutzungsbedingungen',
      },
      about: {
        title: 'Über uns',
        subheading: `<b>covidmemory.lu</b> ist eine freie und offene Online-Plattform,
        die Erinnerungen an die COVID19 Krise in Luxemburg sammelt.`,
      },
      notFound: {
        title: 'Not found',
        subheading: '',
      },
      memories: {
        title: 'Erinnerungen.',
        total: 'loading... | 1 memory | {n} Erinnerungen',
      },
      memory: {
        share: 'Help us collecting memories! covidmemory.lu',
      },
      map: {
        title: 'Karte.',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'Start',
      memories: 'Erinnerungen',
      about: 'Über uns',
      intro: 'Hintergrund',
      create: 'beitragen!',
      map: 'Karte',
      terms: 'Nutzungsbedingungen',
      privacy: 'Privacy',
    },
    languages: 'Sprachen',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
      lb: 'Lëtzebuergesch',
    },
    welcome: 'Willkommen!',
  },
  lb: {
    actions: {
      accept: 'acceptéieren',
      download: 'download original file',
      submit: 'submit',
      createMemory: 'Bäitrag areechen',
      previousMemory: 'virescht Erënnerung:',
      nextMemory: 'next Erënnerung:',
      share: 'deelen:',
    },
    createdBy: 'vum <b>{creator}</b>',
    usefulLinks: 'direkt Linken:',
    project: {
      title: '#covidmemory',
      more: '(méi iwwert de Projet)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send',
      },
      intro: {
        title: 'COVID-19 Erënnerungen',
        subheading: `Plattform fir d’Sammele vu Fotoen, Videoen a
        Geschichten iwwer d’COVID-19 Kris zu Lëtzebuerg.`,
        covidAlert: `Falls dir Froen zum COVID-19 oder ärer eegener Gesondheet hutt,
        da besicht w.e.g. d’Säit
        <a target="_blank" href="https://coronavirus.gouvernement.lu/de.html">covid19.lu</a>`,
      },
      terms: {
        title: 'Benotzungskonditiounen',
      },
      about: {
        title: 'Iwwert eis',
        subheading: '<b>covidmemory.lu</b> ass eng fräi an oppen Online Plattform déi Erënnerunge vun der COVID19 Kris zu Lëtzebuerg sammelt.',
      },
      notFound: {
        title: 'Nët fonnt',
        subheading: '',
      },
      memories: {
        title: 'Erënnerungen.',
        total: 'lueden... | 1 Erënnerung | {n} Erënnerungen',
      },
      memory: {
        share: 'Hëlleft eis Erënnerungen ze sammelen! covidmemory.lu',
      },
      map: {
        title: 'Kaart.',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'Start',
      memories: 'Erënnerungen',
      about: 'Iwwert eis',
      intro: 'Hannergrond',
      create: 'bäidroen!',
      map: 'Kaart',
      terms: 'Benotzungskonditiounen',
      privacy: 'Privatsphär',
    },
    languages: 'Sproochen',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
      lb: 'Lëtzebuergesch',
    },
    welcome: 'Wëllkomm!',
  },
};
