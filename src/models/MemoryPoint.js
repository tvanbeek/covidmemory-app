import { getValue } from '@/models/utils';

export default class MemoryPoint {
  constructor({
    id = 0,
    title = '',
    creator = '',
    date = new Date(),
    dateCreated = new Date(),
    dateModified = new Date(),
    basedNear = {},
    thumbnail = '',
  } = {}) {
    this.id = parseInt(id, 10);
    this.title = String(title);
    this.creator = String(creator).trim();
    this.date = date;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
    this.thumbnail = thumbnail.medium;

    if (typeof basedNear !== 'object') {
      try {
        this.basedNear = JSON.parse(basedNear);
      } catch (e) {
        console.warn(
          'error parsing basedNear, skipping for object id:',
          this.id, 'with value: ', basedNear,
        );
        this.basedNear = {};
      }
    } else {
      this.basedNear = basedNear;
    }
  }

  static create(item) {
    console.info('MemoryPoint.create', item);
    return new MemoryPoint({
      id: item['o:id'],
      title: item['o:title'],
      dateCreated: new Date(getValue(item, 'o:created')),
      dateModified: new Date(getValue(item, 'o:modified')),
      date: new Date(getValue(item, 'dcterms:created')),
      creator: getValue(item, 'dcterms:creator'),
      basedNear: getValue(item, 'foaf:based_near'),
      thumbnail: item['o:media'].length > 0 ? item['o:media'][0]['o:thumbnail_urls'] : '',
    });
  }

  getDate() {
    let date = new Date();
    if (this.hasDate()) {
      date = this.date;
    } else if (this.dateCreated instanceof Date && !Number.isNaN(this.dateCreated.getTime())) {
      date = this.dateCreated;
    }
    return date;
  }

  hasDate() {
    return this.date instanceof Date && !Number.isNaN(this.date.getTime());
  }

  hasCreator() {
    return this.creator.length > 0;
  }

  hasPlace() {
    return this.place.length > 0;
  }

  hasBasedNear() {
    return Object.keys(this.basedNear).length > 0;
  }

  getFormattedDate() {
    return this.getDate().toISOString().split('T')[0];
  }

  getFormattedBasedNear() {
    let lat = 0;
    let lng = 0;
    let placeName = '';
    // let placeType = '';

    if (this.basedNear.result && this.basedNear.result.place_name) {
      [lng, lat] = this.basedNear.result.geometry.coordinates;
      placeName = this.basedNear.result.place_name;
      // placeType = this.basedNear.result.place_type.join(', ');
    } else if (this.basedNear.geometry) {
      if (this.basedNear.geometry.coordinates) {
        [lng, lat] = this.basedNear.geometry.coordinates;
        placeName = this.basedNear.place_name;
        // placeType = this.basedNear.place_type.join(', ');
      }
    }

    if (placeName.length) {
      return `
        <a href="https://www.openstreetmap.org/?mlat=#map=13/${lat}/${lng}">
          ${placeName}
        </a>`;
    }
    return '';
  }

  getCoordinates({
    flatten,
  } = {}) {
    let lat = 0;
    let lng = 0;
    if (this.basedNear.result) {
      [lng, lat] = this.basedNear.result.geometry.coordinates;
    } else if (this.basedNear.geometry) {
      [lng, lat] = this.basedNear.geometry.coordinates;
    }
    if (flatten) {
      return [lng, lat];
    }
    return { lng, lat };
  }
}
